<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $Sheep = new Animal("Shaun");
    echo "Name : ".$Sheep->name . "<br>";
    echo "Legs : ".$Sheep->leg . "<br>";
    echo "Cold Blooded : ".$Sheep->coldBlooded . "<br>";

    echo "<br>";

    $kodok = new Frog("Buduk");
    echo "Name : ".$kodok->name . "<br>";
    echo "Legs : ".$kodok->leg . "<br>";
    echo "Cold Blooded : ".$kodok->coldBlooded . "<br>";
    $kodok->jump();

    echo "<br>";
    echo "<br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name : ".$sungokong->name . "<br>";
    echo "Legs : ".$sungokong->leg . "<br>";
    echo "Cold Blooded : ".$sungokong->coldBlooded . "<br>";
    $sungokong->yell();
?>